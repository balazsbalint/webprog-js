Webprogramozás beadandó

Kötelező elvárások

- [x] A "További elvárások" részben szereplő README.md fájl megfelelően kitöltve szerepel a feltöltött csomagban
- [x] A játéktábla helyesen megjelenik
- [x] A játéktáblán megjelennek a játékosok bábui
- [x] A kezdőállás helyesen megjelenik a játék elején
- [x] Az aktuális játékos ki tud jelölni egy bábut
- [x] A kijelölt bábu tud egyet lépni előre átlósan
- [x] A kijelölt bábuval lehet egyszer ütni, ha az szabályos
- [x] Ha az egyik játékos leüti a másiknak az összes bábuját, akkor nyer

További elvárások

- [x] Ha az egyik játékos már nem tud szabályosan lépni, akkor a másik játékos nyer
- [x] Van lehetőség megszüntetni egy már kijelölt bábu kijelölését
- [x] Ha egy bábu már az utolsó sorban van, akkor nem lehet kijelölni
- [x] Ütés után van lehetőség újabb ütésre, ha az szabályos
- [x] Ütés után lehet úgy dönteni, hogy nem ütünk tovább, még akkor is, ha erre lehetőségünk lenne
- [x] A játékban látszik, hogy éppen ki következik
- [x] Ki van emelve az a mező, ami fölé visszük az egeret
- [x] Bármelyik játékos nyer, az egyértelműen látszik a felületen
- [x] Egy bábu kijelölésekor egyértelműen jelölve vannak azok a mezők, ahova léphetünk
- [ ] A játék játszható számítógép ellen

