const players = {
    RED: 'red',
    GREEN: 'green',
    EMPTY: 'empty',
}

let act = players.EMPTY;
let redpoints = 0;
let greenpoints = 0;

let field = [
    [0,1,0,1,0,1,0,1],
    [1,0,1,0,1,0,1,0],
    [0,1,0,1,0,1,0,1],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [2,0,2,0,2,0,2,0],
    [0,2,0,2,0,2,0,2],
    [2,0,2,0,2,0,2,0]
]
let utott = false;
let onAction = false;
let cells = document.querySelectorAll('td');
let currentPlace = {x: -1,y: -1};

document.getElementById('nwg').addEventListener('click',()=>{
    redpoints = 0;
    greenpoints = 0;
    players2 = players.EMPTY;
    utesutan = false;
    field = [
        [0,1,0,1,0,1,0,1],
        [1,0,1,0,1,0,1,0],
        [0,1,0,1,0,1,0,1],
        [0,0,0,0,0,0,0,0],
        [0,0,0,0,0,0,0,0],
        [2,0,2,0,2,0,2,0],
        [0,2,0,2,0,2,0,2],
        [2,0,2,0,2,0,2,0]
    ]
    utott = false;
    currentPlace.x = -1;
    currentPlace.y = -1;
    let rand = Math.floor(Math.random()*2);
    if(rand == 0){
        act = players.RED;
        document.getElementById('playerinfo').innerHTML = 'A játék elkezdődött te következel: Piros';
    }else if(rand == 1){
        act = players.GREEN;
        document.getElementById('playerinfo').innerHTML = 'A játék elkezdődött te következel: Zöld';
    }
    refreshTable();
    for(cell of cells){
        cell.addEventListener('click',kijeloles);
        if(cell.classList.contains("active")) cell.classList.remove("active");
        if(cell.classList.contains("purple")) cell.classList.remove("purple");
        cell.addEventListener('mouseenter',mousent);
        cell.addEventListener('mouseleave',mouseleave);
    }
});

function mousent(e){
    if(!e.currentTarget.classList.contains('active')){
        e.currentTarget.classList.add('blue');
    }
}
let player2 = players.EMPTY;
let utesutan = false;

function mouseleave(e){
    if(e.currentTarget.classList.contains('blue')){
        e.currentTarget.classList.remove('blue');
    }
}
let changed = false;
function kijeloles(event){
    let value;
    let x = Number(event.currentTarget.id[0]);
    let y = Number(event.currentTarget.id[1]);
    if(act == players.RED){
        value = 1;
    }else if(act == players.GREEN){
        value = 2;
    }

    if(field[x][y] == value){
       
        if(onAction){
            if(x == currentPlace.x && currentPlace.y == y){
                event.currentTarget.classList.remove("active");
                currentPlace.x = -1;
                currentPlace.y = -1;
                onAction = false;
            }else{
                event.currentTarget.classList.add("active");
                
                for(c of cells){
                    if(currentPlace.x == Number(c.id[0]) && currentPlace.y == Number(c.id[1])){
                        c.classList.remove("active");
                       
                    }
                    if(c.classList.contains("purple")){
                        c.classList.remove("purple");
                    }
                }
                currentPlace.x = x;
                currentPlace.y = y;
            }
        }else{
            if(act == players.RED){
                if(x == 7){

                }else{
                    onAction = true;
                    currentPlace.x = x;
                    currentPlace.y = y;
                    event.currentTarget.classList.add("active");
                }
            }else if(act == players.GREEN){
                if(x == 0){

                }else{
                    onAction = true;
                    currentPlace.x = x;
                    currentPlace.y = y;
                    event.currentTarget.classList.add("active");
                }
            }
        }
    }else if(field[x][y] == 0){
        elorelep();
        ut();
    }
    lehetosegek();
    
    if(utott){
        if(!utesutan){
            if (tudeutni()){
                utesutan = true;
                players2 = act;
                if(act == players.GREEN) act =players.RED;
                else if (act == players.RED) act = players.GREEN;
                document.getElementById("skip").innerHTML = `<button type="button" id="next">Ütés kihagyása</button>`;
                document.getElementById("next").addEventListener('click',skipButton);
            }else{
                utott = false;
            }
        }else{
            if(act == players.GREEN) act =players.RED;
            else if (act == players.RED) act = players.GREEN;
            if(!tudeutni()){
                document.getElementById("skip").innerHTML = "";
                utesutan = false;
                utott = false;
            }else{
                if(act != players2){
                    document.getElementById("skip").innerHTML = "";
                    utesutan = false;
                    utott = false;
                }
            }
            if(act == players.GREEN) act =players.RED;
            else if (act == players.RED) act = players.GREEN;
        } 
    }
    
    refreshTable();
    jatekVege();
    tudelepni();
    
}

function skipButton(){
    document.getElementById("skip").innerHTML = "";
    utesutan = false;
    utott = false;
    if(act == players.GREEN) act =players.RED;
    else if (act == players.RED) act = players.GREEN;
    refreshTable();
}

function tudeutni(){
    if(utott){
            for(let i= 0; i< 8; ++i){
                for(let j= 0; j< 8; ++j){
                    if(act == players.GREEN){
                        if(field[i][j] == 1){
                            if(i+1 < 8 && j+1 <8){
                                
                                if(field[i+1][j+1] == 2){
                                    
                                    if(i+2 != 8 && j+2 != 8){
                                        
                                        if(field[i+2][j+2] == 0){
                                            return true;
                                        }
                                    }
                                }
                            }if(i+1 < 8 && j-1 >-1){
                                if(field[i+1][j-1] == 2){
                                    if(i+2 != 8 && j-2 != 8){
                                        if(field[i+2][j-2] ==0){ 
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if(act == players.RED){
                        if(field[i][j] ==  2){
                            
                            if(i-1 > -1 && j+1 <8){
                                
                                if(field[i-1][j+1] == 1){
                                    
                                    if(i-2 != -1 && j+2 != 8){
                                        if(field[i-2][j+2] ==0){
                                            return true;
                                        }
                                    }
                                }
                            }
                            if(i-1 >-1 && j-1 >-1){
                                if(field[i-1][j-1] == 1){
                                    
                                    if(i-2 != -1 && j-2 != -1){
                                        
                                        if(field[i-2][j-2] ==0){
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }


                }
            }
            return false;    
        
    }else{
        return false;
    }
}


function tudelepni(){
    let lehet = false;
    for(let i= 0; i<8; ++i){
        for(let j= 0; j<8; ++j){
            if(act == players.RED){
                if(field[i][j] == 1){
                    if(i+1 <8 && j-1>-1){
                        if(field[i+1][j-1] ==0 || field[i+1][j-1] ==2){
                            lehet = true;
                        }
                    }else if(i+1 <8 && j+1<8){
                        if(field[i+1][j+1] ==0 || field[i+1][j+1] ==2){
                            lehet = true;
                        }
                    }
                }
            }else if(act == players.GREEN){
                if(field[i][j] == 2){
                    if(i-1 >-1 && j-1>-1){
                        if(field[i-1][j-1] ==0 || field[i-1][j-1] ==1){
                            lehet = true; 
                        }
                    }else if(i-1 >-1 && j+1<8){
                        if(field[i-1][j+1] ==0 || field[i-1][j+1] ==1){
                            lehet = true;
                        }
                    }
                }
                
            }
        }
    }
    if(lehet == false){
        if(act == players.GREEN){
            document.getElementById("playerinfo").innerHTML = "A játék véget ért, mert a zöld játékos nem tudott lépni!";
            document.getElementById("playerinfo").innerHTML += `<br>`;
            document.getElementById("playerinfo").innerHTML += "A piros győzött!";
            if(currentPlace.x != -1){
                document.getElementById(""+currentPlace.x+currentPlace.y).classList.remove("active");
            }
        }else if(act == players.RED){
            document.getElementById("playerinfo").innerHTML = "A játék véget ért, mert a piros játékos nem tudott lépni!";
            document.getElementById("playerinfo").innerHTML += `<br>`;
            document.getElementById("playerinfo").innerHTML += "A zöld győzött!";
            if(currentPlace.x != -1){
                document.getElementById(""+currentPlace.x+currentPlace.y).classList.remove("active");
            }
        }
        for(c of cells){
            c.removeEventListener("click",kijeloles);
        }
    }
   
}

function jatekVege(){
    if(redpoints == 12){
        document.getElementById("playerinfo").innerHTML = "A játék véget ért, a piros játékos nyert";
        for(c of cells){
            c.removeEventListener("click",kijeloles);
        }
    }else if(redpoints == 12){
        document.getElementById("playerinfo").innerHTML = "A játék véget ért, a zöld játékos nyert";
        for(c of cells){
            c.removeEventListener("click",kijeloles);
        }
    }
}

function lehetosegek(){
    if(onAction){
        if(act == players.GREEN ){
            if(currentPlace.x-1 != -1 && (currentPlace.y+1 !=8 || currentPlace.y-1 != 0) ){
                if(currentPlace.y+1 !=8 ){
                    if(!utesutan){
                        if(field[currentPlace.x-1][currentPlace.y+1] ==0){
                            document.getElementById(""+(currentPlace.x-1) +(currentPlace.y+1)).classList.add("purple");
                        }
                    }
                    
                    if(field[currentPlace.x-1][currentPlace.y+1] ==1){
                        if(currentPlace.x-2 != -1 && currentPlace.y+2 != 8){
                            if(field[currentPlace.x-2][currentPlace.y+2] == 0){
                                document.getElementById(""+(currentPlace.x-2) +(currentPlace.y+2)).classList.add("purple");
                            } 
                        }
                        
                    }
                }
                if(currentPlace.y-1 != -1){
                    if(!utesutan){
                        if(field[currentPlace.x-1][currentPlace.y-1] ==0){
                            document.getElementById(""+(currentPlace.x-1) +(currentPlace.y-1)).classList.add("purple");
                        }
                    }
                    
                    if(field[currentPlace.x-1][currentPlace.y-1] ==1){
                        if(currentPlace.x-2 != -1 && currentPlace.y-2 != -1){
                            if(currentPlace.x-2 != -1 && currentPlace.y-2 != 8){
                                if(field[currentPlace.x-2][currentPlace.y-2] == 0){
                                    document.getElementById(""+(currentPlace.x-2) +(currentPlace.y-2)).classList.add("purple");
                                } 
                            }
                        }
                    }
                }
            }
            
        }
        else if(act == players.RED){
            if(currentPlace.x+1 != 8 && (currentPlace.y+1 !=8 || currentPlace.y-1 != 0) ){
                if(currentPlace.y+1 !=8 ){
                    if(!utesutan){
                        if(field[currentPlace.x+1][currentPlace.y+1] ==0){
                            document.getElementById(""+(currentPlace.x+1) +(currentPlace.y+1)).classList.add("purple");
                        }
                    }
                    if(field[currentPlace.x+1][currentPlace.y+1] ==2){
                        if(currentPlace.x+2 != 8 && currentPlace.y+2 != 8){
                            if(field[currentPlace.x+2][currentPlace.y+2] == 0){
                                document.getElementById(""+(currentPlace.x+2) +(currentPlace.y+2)).classList.add("purple");
                            } 
                        }
                        
                    }
                }
                if(currentPlace.y-1 != -1){
                    if(!utesutan){
                        if(field[currentPlace.x+1][currentPlace.y-1] ==0){
                            document.getElementById(""+(currentPlace.x+1) +(currentPlace.y-1)).classList.add("purple");
                        }
                    }
                    
                    if(field[currentPlace.x+1][currentPlace.y-1] ==2){
                        if(currentPlace.x+2 != -1 && currentPlace.y-2 != -1){
                            if(field[currentPlace.x+2][currentPlace.y-2] == 0){
                                document.getElementById(""+(currentPlace.x+2) +(currentPlace.y-2)).classList.add("purple");
                            } 
                        }
                    }
                }
            }
        }
    }
}

function ut(){
    if(onAction){
        let x = Number(event.currentTarget.id[0]);
        let y = Number(event.currentTarget.id[1]);
        if(act == players.RED){
            if(x == currentPlace.x +2 && (y == currentPlace.y -2  || y == currentPlace.y +2)){
                if(y == currentPlace.y-2){
                    if(field[x-1][y+1] == 2){
                        field[currentPlace.x][currentPlace.y] = 0;
                        field[x-1][y+1] = 0;
                        field[x][y] = 1;
                        onAction = false;
                        act = players.GREEN;
                        ++redpoints;
                        utott = true;
                    }
                }else if(y == currentPlace.y +2){
                    if(field[x-1][y-1] == 2){
                        field[currentPlace.x][currentPlace.y] = 0;
                        field[x-1][y-1] = 0;
                        field[x][y] = 1;
                        onAction = false;
                        act = players.GREEN;
                        ++redpoints;
                        utott = true;
                    }
                }
                
            }

        }else if(act == players.GREEN){
            if(x == currentPlace.x -2 && (y == currentPlace.y -2  || y == currentPlace.y +2)){
                if(y == currentPlace.y-2){
                    if(field[x+1][y+1] == 1){
                        field[currentPlace.x][currentPlace.y] = 0;
                        field[x+1][y+1] = 0;
                        field[x][y] = 2;
                        onAction = false;
                        act = players.RED;
                        ++greenpoints;
                        utott = true;
                    }
                }else if(y == currentPlace.y +2){
                    if(field[x+1][y-1] == 1){
                        field[currentPlace.x][currentPlace.y] = 0;
                        field[x+1][y-1] = 0
                        field[x][y] = 2;
                        onAction = false;
                        act = players.RED;
                        ++greenpoints;
                        utott = true;
                    }
                }
                
            }
        }
        
    }
}

function elorelep(){
    if(!utesutan){
        if(onAction){
            let x = Number(event.currentTarget.id[0]);
            let y = Number(event.currentTarget.id[1]);
            if(act == players.RED){
                if(x == currentPlace.x +1 && (y == currentPlace.y -1  || y == currentPlace.y +1)){
                    field[currentPlace.x][currentPlace.y] = 0;
                    field[x][y] = 1;
                    onAction = false;
                    act = players.GREEN;
                }
    
            }else if(act == players.GREEN){
                if(x == currentPlace.x -1 && (y == currentPlace.y -1  || y == currentPlace.y +1)){
                    field[currentPlace.x][currentPlace.y] = 0;
                    field[x][y] = 2;
                    onAction = false;
                    act = players.RED;
                }
            }
        }
    }
    

}

function refreshTable(){
    for(i of cells){
        let x = Number(i.id[0]);
        let y = Number(i.id[1]);
        if(field[x][y] == 2){
            i.innerHTML = `<span class="green-circle"></span>`;
        }else if(field[x][y] == 1){
            i.innerHTML = `<span class="red-circle"></span>`;
        }else if(field[x][y] == 0){
            i.innerHTML = '';
        }
        if(!onAction){
            if(i.classList.contains("active")){
                i.classList.remove("active");
            }
            if(i.classList.contains("purple")){
                i.classList.remove("purple");
            }
            
        }else{
            
        }
    }
    if(act == players.RED){
        document.getElementById('playerinfo').innerHTML = 'A játék elkezdődött te következel: Piros';
    }else if(act == players.GREEN){
        document.getElementById('playerinfo').innerHTML = 'A játék elkezdődött te következel: Zöld';
    }
    document.getElementById("eredmeny").innerHTML = "Zöld játékos pontjainak a száma: "+greenpoints;
    document.getElementById("eredmeny").innerHTML += `<br>`;
    document.getElementById("eredmeny").innerHTML += "Piros játékos pontjainak a száma: "+redpoints;
}

